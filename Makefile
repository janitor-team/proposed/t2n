# Talk2Ntx (t2n) 
# 
# 1) libusb is required (http://libusb.sourceforge.net/)
# 2) define ARCHI var if several architecture are targeted
#    (e.g. linux, sparc, cygwin etc ...) 
#
# N.B. tested on :
#      - pc/linux with g++ 3.4
#      - sparc/solaris with g++ 3.4
#

# options for debian packaging hardening
# include /usr/share/hardening-includes/hardening.make
# CFLAGS=$(shell dpkg-buildflags --get CFLAGS)
# LDFLAGS=$(shell dpkg-buildflags --get LDFLAGS)
# CFLAGS+=$(HARDENING_CFLAGS)
# LDFLAGS+=$(HARDENING_LDFLAGS)

MAIN=t2n

OBJDIR=./obj$(ARCHI)
SRCDIR=./src

all: dynamic static

dynamic : $(OBJDIR) $(OBJDIR)/$(MAIN)  $(OBJDIR)/usbscan
static : $(OBJDIR)/$(MAIN).static

CC=g++
LK=g++
CFLAGS+= -I$(SRCDIR) -I$(OBJDIR) -g -Wall
LDFLAGS+=

LIBS=-lusb -lstdc++

OBJS=$(OBJDIR)/$(MAIN).o \
     $(OBJDIR)/usbmisc.o \
     $(OBJDIR)/usbnxt.o \
     $(OBJDIR)/errors.o \
     $(OBJDIR)/errormng.o \
     $(OBJDIR)/ezargs.o 

$(OBJDIR)/usbscan: $(SRCDIR)/usbscan.c
	gcc $(CFLAGS) $(LDFLAGS) $(SRCDIR)/usbscan.c $(LIBS) -o $(OBJDIR)/usbscan

$(OBJDIR)/$(MAIN) : $(OBJS)
	$(LK) $(LDFLAGS) $(OBJS) $(LIBS) -o $(OBJDIR)/$(MAIN)

$(OBJDIR)/$(MAIN).static : $(OBJS)
	$(LK) -static $(LDFLAGS) $(OBJS) $(LIBS) -o $(OBJDIR)/$(MAIN).static

$(OBJDIR)/%.o : $(SRCDIR)/%.cc 
	$(CC) -c $(CFLAGS) $(SRCDIR)/$*.cc -o $(OBJDIR)/$*.o


$(OBJDIR)/usbmisc.o : $(SRCDIR)/usbmisc.h $(SRCDIR)/errormng.h $(SRCDIR)/errors.h

$(OBJDIR)/usbnxt.o : $(SRCDIR)/usbnxt.h $(SRCDIR)/usbmisc.h $(SRCDIR)/errormng.h \
                     $(SRCDIR)/errors.h

$(OBJDIR)/$(MAIN).o : $(SRCDIR)/usbnxt.h $(SRCDIR)/usbmisc.h $(SRCDIR)/errormng.h \
                      $(SRCDIR)/errors.h $(SRCDIR)/ezargs.h $(SRCDIR)/version.h

$(OBJDIR) :
	mkdir $(OBJDIR)

clean:
	rm -f $(OBJDIR)/*.o

distclean:
	rm -rf $(OBJDIR)

distrib: all static 
	ddir=t2n-`$(OBJDIR)/$(MAIN) -version`-$(ARCHI) ; \
	echo $$ddir ; \
	if [ ! -d $$ddir ] ; then mkdir $$ddir ; fi ; \
	cp $(OBJDIR)/$(MAIN) $$ddir ; strip $(OBJDIR)/$(MAIN) ; \
	cp $(OBJDIR)/$(MAIN).static $$ddir ; strip $(OBJDIR)/$(MAIN).static ; \
#	cp -r hotplug $$ddir ; \
#	cp -r udev $$ddir ; \
	tar zcvf $$ddir.tgz $$ddir ; \
	mkdir -p publish/files ; \
	cp ,htaccess publish/files/.htaccess ; \
	mv $$ddir.tgz publish/files ; \
	sddir=t2n-`$(OBJDIR)/$(MAIN) -version`.src ; \
	echo $$sddir ; \
	if [ ! -d $$sddir ] ; then mkdir $$sddir ; fi ; \
	cp README $$sddir ; \
	cp COPYING $$sddir ; \
	cp COPYING.LESSER $$sddir ; \
	cp Makefile $$sddir ; \
	cp -r $(SRCDIR) $$sddir ; \
	cp -r udev $$sddir ; \
	zedate=`date  +"%Y.%m.%d"`; \
	cat publish/in.index.html | \
		sed -e s/BINDIST/$$ddir.tgz/g | \
      sed -e s/SRCDIST/$$sddir.tgz/g -e s/ZEDATE/$$zedate/ > publish/index.html ; \
	tar zcvf $$sddir.tgz $$sddir ; mv $$sddir.tgz publish/files
	rsync -avz publish/ /import/www/PEOPLE/raymond/edu/lego/t2n/ 
