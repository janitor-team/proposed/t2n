/* ----- Copyright 2007 CNRS (VERIMAG) -------------------------------------
--------------------------------------------------------------------------------
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
--------------------------------------------------------------------------------
 * Projet Talk to NXT
 * -------------------------------------------------------------------------
 * module   : version.h 
 *
 * date     : 2012-01-03 
 * auteurs  : Pascal Raymond
 *--------------------------------------------------------------------------
 * nature   : current version of the tool 
 *  
 *--------------------------------------------------------------------------
 * Modifs   :
 * -----------------------------------------------------------------------*/

//Project-Version
#define TOOLNAME "talk2nxt"
#define MAJOR "0"
#define MINOR "6"
//0.3 -> some bugs, -rm option
//0.4 -> compile on linux 64
//0.5 -> patch by Petter Reinholdtsen
//0.6 -> bug argv with static/64 bits
#define VERSION "0.6"

